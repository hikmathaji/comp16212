public class LotteryTestD
{
  public static void main(String[] args)
  {

    SpeedController speedController
    = new SpeedController(SpeedController.HALF_SPEED);

    LotteryGUI gui = new LotteryGUI("TV Studio", speedController);

    //creating new magic worker
    MagicWorker dumbledore = new MagicWorker("Albus Dumbledore");
    gui.addPerson(dumbledore);
    //creating another magic worker
    MagicWorker merlin = new MagicWorker("Merlin");
    gui.addPerson(merlin);
    //creating first game for first magic worker
    Game game1 = new Game("Lott O'Luck Larry", 49,
              "Slippery's Mile", 7);
    gui.addGame(game1);

    dumbledore.fillMachine(game1);
    //ejecting balls from the first machine
    for (int count = 1; count <= game1.getRackSize(); count ++)
    {
      game1.ejectBall();
      speedController.delay(40);
    } // for
    //creating the second game for the second magic worker
    Game game2 = new Game("Lott O'Luck Larry 2", 49,
              "Slippery's Mile 2", 7);
    gui.addGame(game2);


    merlin.fillMachine(game2);

    //ejecting balls from the second machine
    for (int count = 1; count <= game1.getRackSize(); count ++)
    {
      game2.ejectBall();
      speedController.delay(40);
    } // for
    //sorting the racks
    game1.rackSortBalls();
    game2.rackSortBalls();
  }
}