// This class is for testing the date class

public class TestRelativeDates
{
  public static void main(String[] args)
  {
    try
    {
      // today's date
      Date date = new Date("29/2/2012");
      //tomorrow's date
      Date tomorrow = date.addDay();
      System.out.println(tomorrow.toString());
      //yesterday's date
      Date yesterday = date.subtractDay();
      System.out.println(yesterday.toString());
      //the date after a month
      Date nextMonth = date.addMonth();
      System.out.println(nextMonth.toString());
      //the date month before
      Date prevMonth = date.subtractMonth();
      System.out.println(prevMonth.toString());
      //the date after a year
      Date nextYear = date.addYear();
      System.out.println(nextYear.toString());
      // the date year before 
      Date prevYear = date.subtractYear();
      System.out.println(prevYear.toString());

    }
    catch(Exception exception)
    {
    System.out.println("Something unforseen has happened...");
    }
  }
}