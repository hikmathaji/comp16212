// A class that holds house number and person name details and provides 
// compareTo method in order to sort the collections
public class DeliveryHouseDetails implements Comparable<DeliveryHouseDetails>
{
  // class variables
  int houseNumber;
  String personNameDetails;

  public DeliveryHouseDetails(int requiredHouseNumber, 
                              String requiredPersonNameDetails)
  {
    houseNumber = requiredHouseNumber;
    personNameDetails = requiredPersonNameDetails;
  }// DeliveryHouseDetails

  // A method that compares two DeliveryHouseDetails object.
  public int compareTo(DeliveryHouseDetails other)
  {
    if(houseNumber % 2 == 1 && other.houseNumber % 2 == 1)
      return houseNumber - other.houseNumber;
    else if (houseNumber % 2 == 0 && other.houseNumber % 2 == 0)
      return other.houseNumber - houseNumber;
    else if (houseNumber % 2 == 1)
      return -1;
    else
      return 1;
  }// compareTo

  @Override
  public boolean equals(Object other)
  {
    if (other instanceof DeliveryHouseDetails)
      return houseNumber == ((DeliveryHouseDetails)other).houseNumber;
    else
      return super.equals(other);
  }//equals

  // returns personNameDetails
  public String getPersonNameDetails()
  {
    return personNameDetails;
  }// getPersonNameDetails

}// class