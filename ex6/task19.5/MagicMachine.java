public class MagicMachine extends Machine
{
	public MagicMachine(String name, int size)
	{
		super(name, size);
	}
	public String getType()
	{
		return "Dramatic Lottery machine";
	}
	public MagicBall ejectBall()
	{
		if(getNoOfBalls() <= 0)
			return null;
		else
		{
			int ejectedBallIndex = (int)(Math.random() * getNoOfBalls());
			MagicBall ejectedBall = getBall(ejectedBallIndex);
			swapBalls(ejectedBallIndex, getNoOfBalls() -1);
			removeBall();
			return ejectedBall;
		}
	}

}