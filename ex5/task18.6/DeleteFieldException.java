import java.io.IOException;

public class DeleteFieldException extends IOException
{
	public DeleteFieldException(String message)
	{
		super(message);
	}
}