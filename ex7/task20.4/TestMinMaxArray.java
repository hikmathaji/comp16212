import java.io.IOException;
// A class to test MinMaxArray class
// finds min and max of the given numbers as arguments
public class TestMinMaxArray
{
  public static void main(String[] args)
  {
    // creating int array
    int[] a = new int[10];
    for(int i = 0; i<10; i++)
      a[i] = i+i; 
    try
    {
      if(args.length == 0)
        throw new IllegalArgumentException("No arguments given");
      System.out.println("Minimum is " + 
                      MinMaxArray.findMinMax(args).getFirst().toString());
    
      System.out.println("Maximum is " + 
                      MinMaxArray.findMinMax(args).getSecond().toString());
    }
    catch(Exception e)
    {
      System.err.println(e);
    }
   
  }// main
}// class
