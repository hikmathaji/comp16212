// Sorts house addresses and writes them into a file in a particular order
// Input file is the first argument, output file is the second argument

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.Iterator;
import java.util.Collections;

public class StreetOrder
{
  public static void main(String[] args)
  {
    // Initializing input and output
    BufferedReader input = null;
    PrintWriter output = null;

    try
    {
      // checking the number of arguments
      if(args.length !=2)
      {
        throw new IllegalArgumentException("There must be two arguments");
      }// if
      // assigning input and output by given arguments
      input = new BufferedReader(new FileReader(args[0]));
      output = new PrintWriter(new FileWriter(args[1]));
      // creating a tree set that holds the details
      TreeSet<DeliveryHouseDetails> addresses = 
        new TreeSet<DeliveryHouseDetails>();
      // initializing some local variables
      String currentLine;
      int index = 1;
      // getting input
      while((currentLine = input.readLine()) != null)
      {
        // creating a new object to be inserted into addresses
        DeliveryHouseDetails deliveryHouseDetails = 
        new DeliveryHouseDetails(index, currentLine);
        // add the new created object to the list
        addresses.add(deliveryHouseDetails);
        index++;
      }// while
      // create iterator to iterate through the tree
      Iterator<DeliveryHouseDetails> addressesIterator = addresses.iterator();
      while(addressesIterator.hasNext())
      {
        output.println(addressesIterator.next().getPersonNameDetails());
      }// while
    }
    catch(Exception e)
    {
      System.err.println(e);
    }// catch
    finally
    {
      try
      {
        if (input != null)
        {
          input.close();
        }// if
      }// try
      catch(Exception e)
      {
        System.err.println("couldn't close the input "+e);
      }// catch
      try
      {
        if (output != null)
        {
          output.close();
        }// if
      }// try
      catch(Exception e)
      {
        System.err.println("couldn't close the output "+e);
      }// catch
    }// finally
  }// main
}// class
