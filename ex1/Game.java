public class Game
{

// ----------------------------------------------------------------------
// Part a: the score message

  private String scoreMessage;

  public String getScoreMessage()
  {
    return scoreMessage;
  } // getScoreMessage


  public void setScoreMessage(String message)
  {
    scoreMessage = message;
  } // setScoreMessage


  public String getAuthor()
  {
    return "Hikmat Hajiyev";
  } // getAuthor

// ----------------------------------------------------------------------
// Part b: constructor and grid accessors

  private final int gridSize;
  private final Cell[][] gridCell;

  public Game(int requiredGridSize)
  {
    gridSize = requiredGridSize;
    gridCell = new Cell[getGridSize()][getGridSize()];
    for(int i = 0; i < gridSize; i++)
      for(int j = 0; j< gridSize; j++)
        gridCell[i][j] = new Cell();
   } // Game


  public int getGridSize()
  {
    return gridSize;
  } // getGridSize


  public Cell getGridCell(int x, int y)
  {
    return gridCell[x][y];
  } // getGridCell


// ----------------------------------------------------------------------
// Part c: initial game state

// Part c-1: setInitialGameState method

  public void setInitialGameState(int requiredTailX, int requiredTailY,
                                  int requiredLength, int requiredDirection)
  {
    for(int i = 0; i < gridSize; i++)
      for(int j = 0; j< gridSize; j++)
        gridCell[i][j].setClear();
    placeFood();
    if(treesEnabled)
      placeTree();
    placeSnake(requiredTailX, requiredTailY, requiredLength, requiredDirection);
    resetCountdown();
  } // setInitialGameState


// ----------------------------------------------------------------------
// Part c-2 place food

  private int foodLocationX, foodLocationY;

  public void placeFood()
  {
    do{
      foodLocationX = (int) (Math.random() * getGridSize());
      foodLocationY = (int) (Math.random() * getGridSize());
    }while(getGridCell(foodLocationX, foodLocationY).getType()!=Cell.CLEAR);
    getGridCell(foodLocationX, foodLocationY).setFood();
  }//placeFood



  public int getFoodLocationX()
  {
    return foodLocationX;
  }//getFoodLocationY

  public int getFoodLocationY()
  {
    return foodLocationY;
  }//getFoodLocationY

  // ----------------------------------------------------------------------
  // Part c-3: place snake
  public int snakeDirection, snakeTailX, snakeTailY, snakeLength, 
             snakeHeadX, snakeHeadY;

  public void placeSnake(int requiredTailX, int requiredTailY, 
                        int requiredLength, int requiredDirection )
  {
    int currentCellX, currentCellY;

    snakeDirection = requiredDirection;

    getGridCell(requiredTailX, requiredTailY).setSnakeTail(Direction.opposite(
                                              snakeDirection),snakeDirection);

    snakeTailX = requiredTailX;
    snakeTailY = requiredTailY;
    currentCellX = snakeTailX;
    currentCellY = snakeTailY;

    for(int i = 0; i<requiredLength-1; i++)
    {
      currentCellX += Direction.xDelta(snakeDirection);
      currentCellY += Direction.yDelta(snakeDirection);

      getGridCell(currentCellX, currentCellY).setSnakeBody(Direction.opposite(
                                              snakeDirection),snakeDirection );
    }//for
    snakeHeadX = currentCellX;
    snakeHeadY = currentCellY;
    getGridCell(snakeHeadX, snakeHeadY).setSnakeHead(Direction.opposite(
                                        snakeDirection),snakeDirection );
    
  }//placeSnake

// ----------------------------------------------------------------------
// Part d: set snake direction


  public void setSnakeDirection(int requiredDirection)
  {

    if(requiredDirection == getGridCell(snakeHeadX, 
                                        snakeHeadY).getSnakeInDirection())
      setScoreMessage("You cannot move like that...");
    else
  	{
      getGridCell(snakeHeadX,   
                  snakeHeadY).setSnakeOutDirection(requiredDirection);
      snakeDirection = requiredDirection;;
    }//else
    
  } // setSnakeDirection


// ----------------------------------------------------------------------
// Part e: snake movement

// Part e-1: move method


  public void move(int moveValue)
  {
    boolean thereWasFood = false;
    int newHeadX, newHeadY;
    if(!getGridCell(snakeHeadX, snakeHeadY).isSnakeBloody())
    {
      newHeadX = snakeHeadX + Direction.xDelta(getGridCell(snakeHeadX, 
                                        snakeHeadY).getSnakeOutDirection());
      newHeadY = snakeHeadY + Direction.yDelta(getGridCell(snakeHeadX, 
                                        snakeHeadY).getSnakeOutDirection());
      if(checkForCrash(newHeadX, newHeadY))
      {
        if(getGridCell(newHeadX, newHeadY).getType() == Cell.FOOD)
        {
          thereWasFood = true;
        }//if

        moveSnakeHead(newHeadX, newHeadY);
        
        if(thereWasFood)
        {
          eatFood(moveValue);
        }//if
        else
        {
          int newTailX = snakeTailX + Direction.xDelta(getGridCell(snakeTailX, 
                                      snakeTailY).getSnakeOutDirection());
          int newTailY = snakeTailY + Direction.yDelta(getGridCell(snakeTailX, 
                                      snakeTailY).getSnakeOutDirection());
          moveSnakeTail(newTailX, newTailY);
        }//else
      }//if
    }//if
  } // move 


// ----------------------------------------------------------------------
// Part e-2: move the snake head
  public void moveSnakeHead(int newHeadX, int newHeadY)
  {
    getGridCell(snakeHeadX, snakeHeadY).setSnakeBody();
    snakeHeadX = newHeadX;
    snakeHeadY = newHeadY;
    getGridCell(snakeHeadX, snakeHeadY).setSnakeOutDirection(snakeDirection);

    getGridCell(newHeadX, newHeadY).setSnakeHead();
    getGridCell(newHeadX, newHeadY).setSnakeOutDirection(snakeDirection);
    getGridCell(newHeadX, newHeadY).setSnakeInDirection(
                                    Direction.opposite(snakeDirection));
  }//moveSnakeHead
// ----------------------------------------------------------------------
// Part e-3: move the snake tail
  public void moveSnakeTail(int newTailX, int newTailY)
  {
    getGridCell(snakeTailX, snakeTailY).setClear();
    snakeTailX = newTailX;
    snakeTailY = newTailY;
    getGridCell(snakeTailX, snakeTailY).setSnakeTail();
  }//moveSnakeTail

// ----------------------------------------------------------------------
// Part e-4: check for and deal with crashes
  public boolean checkForCrash(int newCellX, int newCellY)
  {//crashed into the wall
    if(newCellX<0||newCellY<0||newCellY>gridSize-1||newCellX>gridSize-1)
    { 
      if(crashCountdown())
      {
        setScoreMessage("Right into the wall");
        getGridCell(snakeHeadX, snakeHeadY).setSnakeBloody(true);
       
      }//if
       return false;
    }//if
    else if(getGridCell(newCellX, newCellY).isSnakeType())//crashed into itself
    {
      if(crashCountdown())
      {
        setScoreMessage("You cannot eat yourself");
        getGridCell(snakeHeadX, snakeHeadY).setSnakeBloody(true);
        getGridCell(newCellX, newCellY).setSnakeBloody(true);
        
      }//if
      return false;
    } //crashed into a tree
    else if(getGridCell(newCellX, newCellY).getType() == Cell.TREE)
    {
      if(crashCountdown())
      {
        setScoreMessage("You crashed into a tree");
        getGridCell(snakeHeadX, snakeHeadY).setSnakeBloody(true);
        getGridCell(newCellX, newCellY).setSnakeBloody(true);
        
      }//if
      return false;
    }//else if
    return true;
  }// checkForCrash


// ----------------------------------------------------------------------
// Part e-5: eat the food
  public void eatFood(int moveValue)
  {
    snakeLength++;
    int gainedScore = moveValue * 
                      ((snakeLength / (gridSize * gridSize / 36 +1))+1);
    if(treesEnabled)
    {
      int finalScore = gainedScore * numberOfTrees;
      setScoreMessage("Raw score: " + gainedScore + ", #trees " + numberOfTrees 
                      + ", actual gain: " + finalScore);
      score += finalScore;
     
      placeTree();
    }//if
    else
    {
      setScoreMessage("+" + gainedScore);
      score += gainedScore;
    }//else
    placeFood();

  }//eatFood

  private static int score = 0;

  public int getScore()
  {
    return score;
  } // getScore


// ----------------------------------------------------------------------
// Part f: cheat


  public void cheat()
  {
    int lostScore = score/2;
    setScoreMessage("-" + lostScore + ", you lost half of your score");
    score -= lostScore;
     for(int i = 0; i < gridSize; i++)
      for(int j = 0; j< gridSize; j++)
        if(getGridCell(i, j).isSnakeBloody())
          getGridCell(i, j).setSnakeBloody(false);

  } // cheat


// ----------------------------------------------------------------------
// Part g: trees

  public int numberOfTrees;
  public static boolean treesEnabled = false;

  public void toggleTrees()
  {
    if(treesEnabled)
    {
      treesEnabled = false;
       for(int i = 0; i < gridSize; i++)
         for(int j = 0; j< gridSize; j++)
          if(getGridCell(i, j).getType() == Cell.TREE)
            getGridCell(i, j).setClear();
    }//if
    else
    {
      treesEnabled = true;
      numberOfTrees = 1;
      placeTree();
    }//else
  } // toggleTrees
  private void placeTree()
  {
    int treeLocationX, treeLocationY;
    do{
      treeLocationX = (int) (Math.random() * getGridSize());
      treeLocationY = (int) (Math.random() * getGridSize());
    }while(getGridCell(treeLocationX, treeLocationY).getType()!=Cell.CLEAR);
    getGridCell(treeLocationX, treeLocationY).setTree();
    numberOfTrees++;
  }

// ----------------------------------------------------------------------
// Part h: crash countdown
  public final int countdownStartValue = 5;
  public int currentCountdownValue;
  private boolean crashCountdown()
  {
    currentCountdownValue --;
    if(currentCountdownValue > 0)
    {
      setScoreMessage("You have " + currentCountdownValue + " moves left");
      return false;
    }//if
    else
    {
      resetCountdown();
      return true;
    }//else
  }

  private void resetCountdown()
  {
    currentCountdownValue = countdownStartValue;
  }

// ----------------------------------------------------------------------
// Part i: optional extras


  public String optionalExtras()
  {
    return "Burn trees\n";
  } // optionalExtras
  private void burnTrees()
  {
    if(snakeDirection == Direction.NORTH)
    {
      for(int currentY = snakeHeadY + 1; currentY>=0; currentY --)
      {
        if(getGridCell(snakeHeadX, currentY).getType()== Cell.TREE)
        {
          getGridCell(snakeHeadX, currentY).setClear();
          numberOfTrees--;
          break;
        }//if
      }//for
    }//if
    else if(snakeDirection == Direction.SOUTH)
    {
      for(int currentY = snakeHeadY - 1; currentY< gridSize; currentY ++)
      {
        if(getGridCell(snakeHeadX, currentY).getType()== Cell.TREE)
        {
          getGridCell(snakeHeadX, currentY).setClear();
          numberOfTrees--;
          break;
        }//if
      }//for
    }//else if
    else if(snakeDirection == Direction.WEST)
    {
      for(int currentX = snakeHeadX - 1; currentX>=0; currentX --)
      {
        if(getGridCell(currentX, snakeHeadY).getType()== Cell.TREE)
        {
          getGridCell(currentX, snakeHeadY).setClear();
          numberOfTrees--;
          break;
        }//if
      }//for
    }//else if
    else if(snakeDirection == Direction.EAST)
    {
      for(int currentX = snakeHeadX + 1; currentX<gridSize; currentX ++)
      {
        if(getGridCell(currentX, snakeHeadY).getType()== Cell.TREE)
        {
          getGridCell(currentX, snakeHeadY).setClear();
          numberOfTrees--;
          break;
        }//if
      }//for
    }//else if
  }// burnTrees


  public void optionalExtraInterface(char c)
  {
    if(c == 'b')
    {
      burnTrees();
    }//if
    else if (c > ' ' && c <= '~')
      setScoreMessage("Key " + new Character(c).toString()
                      + " is unrecognised (try h)");
    

  } // optionalExtraInterface

} // class Game
