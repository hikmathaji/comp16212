// Sorts house addresses and writes them into a file in a particular order
// Input file is the first argument, output file is the second argument

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class StreetOrder
{
  public static void main(String[] args)
  {
    // Initializing input and output
    BufferedReader input = null;
    PrintWriter output = null;

    try
    {
      // checking the number of arguments
      if(args.length !=2)
      {
        throw new IllegalArgumentException("There must be two arguments");
      }// if
      // assigning input and output by given arguments

      input = new BufferedReader(new FileReader(args[0]));
      output = new PrintWriter(new FileWriter(args[1]));
      // creating a list that holds the details
      List<String> addresses = new ArrayList<String>();
      String currentLine;
      // reading the input
      while((currentLine = input.readLine()) != null)
        addresses.add(currentLine);
      // writing the output
      for(int houseIndex = 0; houseIndex<addresses.size(); houseIndex+=2)
        output.println(addresses.get(houseIndex));
      for(int houseIndex = addresses.size()-1 - addresses.size()%2; 
          houseIndex>=1; houseIndex-=2)
        output.println(addresses.get(houseIndex));
    }
    catch(Exception e)
    {
      System.err.println(e);
    }//catch
    finally
    {
      try
      {
        if (input != null)
        {
          input.close();
        }// if
      }// try
      catch(Exception e)
      {
        System.err.println("couldn't close the input "+e);
      }// catch
      try
      {
        if (output != null)
        {
          output.close();
        }// if
      }// try
      catch(Exception e)
      {
        System.err.println("couldn't close the output "+e);
      }// catch
    }// finally
  }//main
}//class
