// A class to check and find duplicate voters. It get input from file which 
// is specified in command line argument and prints to a file. Prints
// duplicates and the number of them.

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class DuplicateVoters
{
  public static void main(String[] args)
  {
    // initializing input and output
    BufferedReader input = null;
    PrintWriter output = null;
    try
    {
      // checking command line arguments
      if(args.length == 0)
        throw new IllegalArgumentException("Please enter input and output");
      else if(args.length == 1)
        throw new IllegalArgumentException("Please enter output file");
      else if(args.length > 2)
        throw new IllegalArgumentException("Too many values");
      // getting input and output
      input = new BufferedReader(new FileReader(args[0]));
      output = new PrintWriter(new FileWriter(args[1]));
      // initializing some variables
      int numberOfDuplicateVoters = 0;
      String currentLine;
      // creating hash set to store lines
      Set<String> myHashSet = new HashSet<String>();
      // reading the input
      while((currentLine = input.readLine()) != null)
      {
        // check if he has already voted
        if(myHashSet.contains(currentLine))
        {
          // if yes, print it and increase number of duplicates
          output.println(currentLine);
          numberOfDuplicateVoters++;
        }// if
        else
        {
          // else remember he has voted
          myHashSet.add(currentLine);
        }// else
        // skip the second lines
        input.readLine();
      }// while
      // printing the number of duplicates
      output.println("There were " + numberOfDuplicateVoters + 
                    " duplicate votes");
    }// try
    // catching exceptions
    catch(Exception exception)
    {
      System.err.println(exception);
    }//catch
    finally
    {
      try
      {
        if(input != null)
        {
          input.close();
        }// if
      }// try
      catch(Exception exception2)
      {
        System.err.println("couldn't close the input " + exception2);
      }// catch
      try
      {
        if (output != null)
        {
          output.close();
        }// if
      }// try
      catch(Exception exception3)
      {
        System.err.println("couldn't close the output " + exception3);
      }// catch
    }// finally
  }
}