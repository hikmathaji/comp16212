import java.io.IOException;

public class CheckSum
{
  public static void main(String[] args)
  {
    try
    {
      int currentByte, checkSum = 0;
      while((currentByte = System.in.read()) != -1)
      {
        
        if(checkSum % 2 == 0)
        {
          checkSum /= 2;
        }//if
        else
        {
          checkSum /= 2;
          checkSum += 0x8000;
        }//else
        checkSum += currentByte;
        if(checkSum >= 0x10000)
          checkSum -= 0x10000;
      }//while
      System.out.println(checkSum);
    }//try
    catch(IOException exception)
    {
      System.out.println(exception.getMessage());
    }//catch
    finally
    {
      try { System.in.close();}
      catch (IOException exception)
      {
        System.err.println("Couldn't close the input " + exception);
      }//catch
    }//finally
  }//main
}//class