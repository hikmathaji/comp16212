public class MagicGame extends Game
{
	public MagicGame(String machineName, int machineSize, String rackName, int rackSize)
	{
		super(machineName, machineSize, rackName, rackSize);
	}
	
	public MagicMachine makeMachine(String machineName, int machineSize)
	{
		return new MagicMachine(machineName, machineSize);
	}
}