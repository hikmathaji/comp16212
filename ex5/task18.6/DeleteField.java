import java.io.*;

public class DeleteField
{
  public static void main(String[] args)
  {
    int fieldToDelete = Integer.parseInt(args[0]);
    BufferedReader input = null;
    PrintWriter output = null;
    try
    {
      if(args.length > 3)
        throw new DeleteFieldException("Too many arguments");
      if(args.length < 2 || args[1].equals("-"))
        input = new BufferedReader(new InputStreamReader(System.in));
      else
        input = new BufferedReader(new FileReader(args[1]));

      if(args.length < 3 || args[2].equals("-"))
        output = new PrintWriter(System.out, true);
      else
      {
        if(new File(args[1]).exists())
          throw new DeleteFieldException("Output file" + args[2] + 
                                        " already exists");
        output = new PrintWriter(new FileWriter(args[2]));
      }//else
      String inputLine = "";
      while ((inputLine = input.readLine()) != null)
      {
        String[] fields = inputLine.split("\t");
        String editedLine = "";
        if(fields.length < fieldToDelete)
          editedLine = inputLine;
        else
        {
          for( int index = 0; index < fieldToDelete -1; index++)
            if(editedLine.equals("")) 
              editedLine = fields[index];
            else
              editedLine +="\t" + fields[index];

          for(int index = fieldToDelete; index < fields.length; index++)
            if(editedLine.equals("")) 
              editedLine = fields[index];
            else
              editedLine += "\t" + fields[index];
        } 
        System.out.println(editedLine);
      }
    }//try
    catch(DeleteFieldException exception)
    {
      System.out.println(exception.getMessage());
    }
    catch(IOException exception2)
    {
      System.out.println(exception2.getMessage());
    }
    catch(Exception exception3)
    {
      System.out.println(exception3.getMessage());
    }
  }
}