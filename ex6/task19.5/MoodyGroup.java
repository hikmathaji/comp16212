// A generic class that contains a collection of some subclass of MooddyPerson
// objects rather like the Conversation class does with the Person

import java.util.ArrayList;
import java.util.List;


public class MoodyGroup<PersonType extends MoodyPerson>
{
  //creating list to hold objects
  public List<PersonType> moodyPeople = new ArrayList<PersonType>();

  //empty constructor, does nothing
  public MoodyGroup()
  {
  }

  private int noOfPeople = 0;
  //returns the size of the list
  public int getSize()
  {
    return noOfPeople;
  }
  private int nextToSetHappy = 0;
  //set people happy
  public void setHappy(boolean b)
  {
    moodyPeople.get(nextToSetHappy).setHappy(b);
    nextToSetHappy = (nextToSetHappy + 1) % noOfPeople;
  } 
  //adds person to the list
  public void addPerson(PersonType person)
  {
    moodyPeople.add(person);
    noOfPeople++;
  }
  // prints the list
  @Override
  public String toString()
  {
    String res = "";
    for(int i = 0; i < noOfPeople; i++)
    {
      if(i == 0)
        res += String.format("%s", moodyPeople.get(i));
      else
        res += String.format("%n%s", moodyPeople.get(i));
    }
    return res;
  }
}