public class DramaticGame extends Game
{
  public DramaticGame(String machineName, int machineSize, String rackName, int rackSize)
  {
    super(machineName, machineSize, rackName, rackSize);
  }//DramaticGame constructor

  public DramaticMachine makeMachine(String machineName, int machineSize)
  {
    return new DramaticMachine(machineName, machineSize);
  }//makeMachine
}