// A class to test MoodyGroup class. Creates some instances of MoodyGroup and
// adds some people to the groups

public class TestMoodyGroup
{
  public static void main(String[] args)
  {
    //creating a teenager group
    MoodyGroup<Teenager> teens = new MoodyGroup<Teenager>();
    //creating teens
    Teenager alice = new Teenager("Alice");
    Teenager bob = new Teenager("Bob");
    //adding teens to the group
    teens.addPerson(alice);
    teens.addPerson(bob);
    teens.setHappy(false);
    //printing the group
    System.out.println("Printing teens...");
    System.out.println(teens.toString());
    teens.setHappy(true);
    System.out.println("Printing teens...");
    System.out.println(teens.toString());
    //creating second group
    MoodyGroup<MoodyPerson> someGroup = new MoodyGroup<MoodyPerson>();
    //creating a worker
    Worker cecil = new Worker("Cecil");
    //adding worker and the already created teenager to the second group
    someGroup.addPerson(cecil);
    someGroup.addPerson(bob);
    someGroup.setHappy(true);
    //printing the second group
    System.out.println("Printing second group...");
    System.out.println(someGroup.toString());
    someGroup.setHappy(false);
    System.out.println("Printing second group...");
    System.out.println(someGroup.toString());
    //printing the first group int order to see the teenager again
    System.out.println("Printing teens...");
    System.out.println(teens.toString());
  }
}