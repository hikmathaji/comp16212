public class FishTankVolume
{
  public static void main(String[] args)
  {
    try
    {
      if(args.length<3)
        throw new IllegalArgumentException("Not enough values");
      if(args.length>3)
        throw new ArrayIndexOutOfBoundsException("You have given " + 
                                                  args.length + " values");
      int width = Integer.parseInt(args[0]);
      int depth = Integer.parseInt(args[1]);
      int height = Integer.parseInt(args[2]);
      if(width <= 0 || depth <= 0 || height <= 0)
        throw new NumberFormatException("You must enter positive values");
      int volume = width * depth * height;
      System.out.println("Volume is: " + volume);
    }//try
    catch(ArrayIndexOutOfBoundsException e)
    {
      System.out.println(e.getMessage());
      System.err.println(e);
    }//ArrayIndexOutOfBoundsException
    catch(NumberFormatException e)
    {
      System.out.println(e.getMessage());
      System.err.println(e);
    }// NumberFormatException
    catch(IllegalArgumentException e)
    {
      System.out.println(e.getMessage());
      System.err.println(e);
    }//IllegalArgumentException
    catch(Exception e)
    {
      System.out.println("Something unforseen has happened");
      System.out.println(e.getMessage());
      System.err.println(e);
    }
  }
}