// prints the permutations of a string given from command line

public class Anagrams
{
	//
	public static char[] chrArray;
	public static char[] chrArrayForPermutation;
	public static boolean[] used;
	public static void main(String[] args)
	{
		try
		{
			// checking if the argument is passed
			if(args.length < 1)
				throw new ArrayIndexOutOfBoundsException("No argument is given");
			chrArray = args[0].toCharArray();
			used = new boolean[args[0].length()];
			chrArrayForPermutation = new char[args[0].length()];
			// initializing the boolean array
			for(int index = 0; index< args[0].length(); index++)
				used[index] = false;
			printPermutations(0);
		}// try
		// catching exception
		catch(Exception exception)
		{
			System.err.println(exception);
		}// catch
	}

	public static void printPermutations(int currentIndex)
	{
		//printing the permutation
		if(currentIndex >= chrArray.length)
			System.out.println(chrArrayForPermutation);
		else
		{
			for(int index = 0; index< chrArray.length; index++)
			{
				if(used[index]==false)
				{
					used[index] = true;
					chrArrayForPermutation[currentIndex] = chrArray[index];
					printPermutations(currentIndex + 1);
					used[index] = false;
				}// if
			}// for
		}//else
	}// printPermutations
}// Anagrams