public class LotteryTestB
{
  public static void main(String[] args)
  {
    SpeedController speedController
      = new SpeedController(SpeedController.HALF_SPEED);
    //creating Lottery GUI
    LotteryGUI gui = new LotteryGUI("TV Studio", speedController);
    //creating and adding people to GUI
    TVHost tvHost = new TVHost("Terry Bill Woah B'Gorne");
    gui.addPerson(tvHost);

    AudienceMember audienceMember1 = new AudienceMember("Ivana Di Yowt");
    gui.addPerson(audienceMember1);

    Punter punter1 = new Punter("Ian Arushfa Rishly Ving");
    gui.addPerson(punter1);

    Psychic psychic = new Psychic("Miss T. Peg de Gowt");
    gui.addPerson(psychic);

    AudienceMember audienceMember2 = new AudienceMember("Norma Lurges");
    gui.addPerson(audienceMember2);

    Director director = new Director("Sir Lance Earl Otto");
    gui.addPerson(director);

    CleverPunter cleverPunter1 = new CleverPunter("Wendy Athinkile-Win");
    gui.addPerson(cleverPunter1);

    Worker worker1 = new TraineeWorker("Jim", 0);
    gui.addPerson(worker1);

    CleverPunter cleverPunter2 = new CleverPunter("Luke Kovthe d'Ville");
    gui.addPerson(cleverPunter2);

    Worker worker2 = new Worker("Merlin");
    gui.addPerson(worker2);
    //creating a dramatic game
    DramaticGame game1 = new DramaticGame("Lott O'Luck Larry", 49,
                          "Slippery's Mile", 7);
    gui.addGame(game1);
  
    worker1.fillMachine(game1);
    speedController.delay(40);

    punter1.speak();
    speedController.delay(40);
    //ejecting balls from machine
    for (int count = 1; count <= game1.getRackSize(); count ++)
    {
      game1.ejectBall();
      audienceMember1.speak();
      cleverPunter1.speak();
    } // for

    game1.rackSortBalls();
    speedController.delay(40);
  }//main
}//class