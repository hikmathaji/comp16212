// Counts the number of words given from standart input
import java.io.IOException;
import java.io.InputStreamReader;

public class WordCount
{
  public static void main(String[] args)
  {
    int currentByte, wordCount = 0;
    try
    {
      InputStreamReader input = new InputStreamReader(System.in);
      char prevByte = '\0';
      boolean firstTime = false;
      while((currentByte = input.read()) != -1)
      {
        //checking the whitespaces
        if(!firstTime)
        {
          if(!Character.isWhitespace((char)currentByte))
            wordCount++;
          firstTime = true;
          prevByte = (char)currentByte;
          continue;
        }
        if(Character.isWhitespace(prevByte) && 
          !Character.isWhitespace((char)currentByte))
          wordCount++;
        prevByte = (char)currentByte;
      }//while
    }
    catch(IOException exception)
    {
      System.out.println(exception.getMessage());
    }//catch
    finally
    {
      System.out.println(wordCount);
      try
      { 
        System.in.close();
      }//try
      catch (IOException exception)
      {
        System.err.println("Couldn't close the input " + exception);
      }//catch
    }//finally
  }//main
}//class