// finds minimum and maximum elements of given array
public class MinMaxArray
{
  public static <ArrayType extends Comparable<ArrayType>> 
  Pair findMinMax(ArrayType[] anArray)
  throws IllegalArgumentException
  {
    // checking the array.
    if(anArray == null)
      throw new IllegalArgumentException("Array must exist");
    if(anArray.length < 1)
      throw new IllegalArgumentException("No elements in the array");
    int max = 0;
    int min = 0;
    // iterating through the array and find minimum and maximum elements
    for(int i = 0; i<anArray.length; i++)
    {
      if (anArray[i].compareTo(anArray[max])>1)
        max = i;
      if (anArray[i].compareTo(anArray[min])<1)
        min = i;
    }// for
    return new Pair<ArrayType, ArrayType>(anArray[min], 
					  anArray[max]);
  }// findMinMax
}// class
