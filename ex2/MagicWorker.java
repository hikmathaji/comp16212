import java.util.ArrayList;
import java.util.List;
import java.awt.Color;

public class MagicWorker extends Worker
{

	public List<MagicBall> ballsEverMade = new ArrayList<MagicBall>();

  public MagicWorker(String name)
  {
    super(name);
  }

  public void doMagic(int spellNumber)
  {
  	for(int i = 0; i<ballsEverMade.size(); i++)
  		ballsEverMade.get(i).doMagic(spellNumber);
  }

  public MagicBall makeNewBall(int requiredValue, Color requiredColor)
  {
  	MagicBall  ball = new MagicBall(requiredValue, requiredColor);
  	ballsEverMade.add(ball);
  	return ball;

  }
  public String getClassHierarchy()
  {
    return "MagicWorker>" + super.getClassHierarchy();
  }//getClassHierarchy

  public MagicWorkerImage makeImage()
  {
    return new MagicWorkerImage(this);
  }

  public String getPersonType()
  {
    return "MagicWorker";
  } // getPersonType
}
