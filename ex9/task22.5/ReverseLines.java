// A class that reverse the input lines from the input and 
// outputs to standart output
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.InputStreamReader;

public class ReverseLines
{
  // initializing input and output
  public static BufferedReader input = null;
  public static PrintWriter output = null;
  // recursive method
  public static void reverseLines(BufferedReader input, PrintWriter output)
  throws IOException
  {
    // initializing the local variable for the current line
    String currentLine = "";
    if((currentLine = input.readLine()) != null)
    {
      // making recursive call
      reverseLines(input, output);
      // printing the result
      output.println(currentLine);
    }// if
  }// reverseLines
  public static void main(String[] args)
  {
    try
    {
      // assinging input and output
      input = new BufferedReader(new InputStreamReader(System.in));
      //input = new BufferedReader(new FileReader(args[0]));
      output = new PrintWriter(System.out, true);
      // calling the recursive function for the first time
      reverseLines(input, output);
    }// try
    // catching the exceptions
    catch(Exception e)
    {
      System.err.println(e);
    }// catch
   
  }// main
}// class
