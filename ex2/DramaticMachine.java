public class DramaticMachine extends Machine
{
  public DramaticMachine(String name, int size)
  {
    super(name, size);
  }//DramaticMachine
  public String getType()
  {
    return "Dramatic Lottery machine";
  }//getType
  public Ball ejectBall()
  {
    if(getNoOfBalls() <= 0)
      return null;
    else
    {
      int ejectedBallIndex = (int)(Math.random() * getNoOfBalls());
      Ball ejectedBall = getBall(ejectedBallIndex);
      for(int index = 0; index<=ejectedBallIndex; index++)
      {
        swapBalls(index, index);
      }//for
      swapBalls(ejectedBallIndex, getNoOfBalls() -1);
      removeBall();
      return ejectedBall;
    }//else
  }//ejectBall
}//class