public class Triple<FirstType, SecondType, ThirdType>
{
	private final FirstType first;
  private final SecondType second;
  private final ThirdType third;


  public Triple(FirstType requiredFirst, SecondType requiredSecond, 
                 ThirdType requiredThird)
  {
    first = requiredFirst;
    second = requiredSecond;
    third = requiredThird;
  }//constructor

  public FirstType getFirst()
  {
    return first;
  }//getFirst

  public SecondType getSecond()
  {
    return second;
  }//getSecond

  public ThirdType getThird()
  {
    return third;
  }//getThird

}//Triple