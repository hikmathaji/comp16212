import java.awt.Color;

public class LotteryTestC
{
  public static void main(String args[])
  {
    SpeedController speedController
      = new SpeedController(SpeedController.HALF_SPEED);

    LotteryGUI gui = new LotteryGUI("TV Studio", speedController);

    Game game1 = new Game("Lott O'Luck Larry",16,
                          "Slippery's Mile", 5);
    gui.addGame(game1);

    //worker1.fillMachine(game1);
    MagicBall[] ball = new MagicBall[15];
    
    ball[0] = new MagicBall(0, Color.blue);
    ball[1] = new MagicBall(1, Color.yellow);
    ball[2] = new MagicBall(2, Color.cyan);
    ball[3] = new MagicBall(3, Color.green);
    ball[4] = new MagicBall(4, Color.red);
    ball[5] = new MagicBall(5, Color.white);
    ball[6] = new MagicBall(6, Color.blue);
    ball[7] = new MagicBall(7, Color.pink);
    ball[8] = new MagicBall(8, Color.white);
    ball[9] = new MagicBall(9, Color.blue);
    ball[10] = new MagicBall(10, Color.yellow);
    ball[11] = new MagicBall(11, Color.cyan);
    ball[12] = new MagicBall(12, Color.red);
    ball[13] = new MagicBall(13, Color.green);
    ball[14] = new MagicBall(14, Color.pink);

    for(int i = 0; i<15; i++)
      game1.machineAddBall(ball[i]);
  } // main
} // class LotteryTest
