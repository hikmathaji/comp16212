// A class to find and return min max and mean of given values
public class IntArrayStats
{
  public static Triple getStats(int[] array)
  {
    //initializing variables
    int min = array[0];
    int max = array[0];
    double mean = 0;
    // finding the min, max and mean
    for(int i = 0 ; i< array.length; i++)
    {
      if(array[i]>max)
        max = array[i];
      if(array[i]<min)
        min = array[i];
      mean += array[i];
    }
    //calculating the mean
    mean = mean / (double)array.length;
    //reutnring the result
    return new Triple(min, max, mean);
  }
}