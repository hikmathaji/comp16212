public class IntArrayStats
{
  public static Triple getStats(int[] array)
  {
    int min = array[0];
    int max = array[0];
    double mean = 0;
    for(int i = 0 ; i< array.length; i++)
    {
      if(array[i]>max)
        max = array[i];
      if(array[i]<min)
        min = array[i];
      mean += array[i];
    }
    mean = mean / (double)array.length;
    return new Triple<Integer, Integer, Double>(min, max, mean);
  }
}