import java.awt.Color;

public class MagicBall extends Ball 
{
  //creating states
  public static final int NORMAL_STATE = 0;
  public static final int INVISIBLE_STATE = 1;
  public static final int FLASHING_STATE = 2;
  public static final int COUNTING_STATE = 3;

  public final MagicBallImage image;
  //creating local variables
  public int currentState;
  public int lastValue;

  public MagicBall(int requiredValue, Color requiredColor)
  {
    super(requiredValue, requiredColor);
    currentState = NORMAL_STATE;
    lastValue = super.getValue();
    image = makeImage();
  }

  public void doMagic(int spellNumber)
  {
    //change to next state, if it's counting state change to normal
    if(spellNumber == 1)
      currentState = (currentState + 1) % 4;
    else if (spellNumber == 2)
      currentState = NORMAL_STATE;
    image.update();
  }//doMagic

  public boolean isVisible()
  {
    if(currentState == INVISIBLE_STATE)
      return false;
    return true;
  }//isVisible

  public boolean isFlashing()
  {
    if(currentState == FLASHING_STATE || currentState == COUNTING_STATE)
      return true;
    return false;
  }//isFlashing

  public int getValue()
  {
    if(currentState == COUNTING_STATE)
      lastValue = lastValue + 1;
    //if value is 100 or more change it to 0
    if(lastValue>99)
      lastValue = 0;
    return lastValue;
  }//getValue

  public MagicBallImage makeImage()
  {
    return new MagicBallImage(this);
  }//makeImage

  public MagicBallImage getImage()
  {
    return image;
  }//getImage
}