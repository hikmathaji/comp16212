import java.io.FileInputStream;
import java.io.IOException;
public class Uuencode
{
  
  public static void writeByteAsChar(int thisByte)
  {
    System.out.print((char) (thisByte == 0 ? 96 : thisByte + 32));
  }//writeByteAsChar

	public static void main(String args[])
  {
    FileInputStream input = null;
    try
    {
      //getting input
      input = new FileInputStream(args[0]);

      int[] lineBytes = new int[45];
      int nextByte = input.read();
      int numberOfBytes = 0;
      System.out.println("begin 600 " + args[0]);
      while(nextByte != -1)
      {
        //getting lines
        while(nextByte != -1 && numberOfBytes < 45 )
        {
          lineBytes[numberOfBytes++] = nextByte;
          nextByte = input.read();
        }//while

        //main encoding
        writeByteAsChar(numberOfBytes);
        for(int byteGroupIndex = 0; byteGroupIndex < numberOfBytes; 
                byteGroupIndex+=3)
        {
          int byte1 = lineBytes[byteGroupIndex] >> 2;
          int byte2 = (lineBytes[byteGroupIndex] & 0x3) << 4 | 
                      (lineBytes[byteGroupIndex + 1] >> 4);
          int byte3 = (lineBytes[byteGroupIndex + 1] & 0xf) << 2 | 
                      (lineBytes[byteGroupIndex + 2] >> 6);
          int byte4 = lineBytes[byteGroupIndex + 2] & 0x3f;
          writeByteAsChar(byte1);
          writeByteAsChar(byte2);
          writeByteAsChar(byte3);
          writeByteAsChar(byte4);
        } 
        numberOfBytes = 0;
        System.out.println();
      }//while

      System.out.println("`\nend");
    }//TRY
    catch(IOException e)
    {
      System.out.println(e.getMessage());
    }//catch
    finally
    {
      try { if (input != null) input.close();}
      catch(IOException exception)
      {
        System.err.println("Couldn't close the input :)" + exception);
      }//catch
    } 
  }
}